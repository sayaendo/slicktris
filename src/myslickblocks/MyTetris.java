package myslickblocks;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class MyTetris extends StateBasedGame {
	
	public static final int MENU_STATE = 1;
	public static final int PLAY_STATE = 2;
	
	public MyTetris() {
		super("TETRIS");
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new MenuState(MENU_STATE));
		this.addState(new PlayState(PLAY_STATE));
		this.getState(MENU_STATE).init(gc, this);
		this.getState(PLAY_STATE).init(gc, this);
		this.enterState(MENU_STATE);
	}

	public static void main(String[] args) throws SlickException {
		AppGameContainer gc = new AppGameContainer(new MyTetris());
		gc.setDisplayMode(636, 632, false);
		gc.start();
	}

}
