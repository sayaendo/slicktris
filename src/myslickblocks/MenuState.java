package myslickblocks;

import java.awt.Font;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MenuState extends BasicGameState {
	
	Image menu, buttons;
	Image startButton, noButton;
	
	float startButtonRotate = 0f;
	float noButtonRotate = 0f;
	
	TrueTypeFont trueTypeFont;
	
	ArrayList<Integer> scores;
	
	int stateID;
	
	public MenuState (int id) {
		this.stateID = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		menu = new Image("data/mytitle.jpg");
		buttons = new Image("data/mymenubuttons.jpg");
		startButton = buttons.getSubImage(20, 0, 270, 101);
		noButton = buttons.getSubImage(65, 116, 200, 100);
		Highscores.INSTANCE.load();
		
		Font font = new Font("Arial", Font.BOLD, 20);
		trueTypeFont = new TrueTypeFont(font, true);
		scores = Highscores.INSTANCE.scores;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		menu.draw(0, 0);
		startButton.draw(338, 190);
		noButton.draw(370, 473);
		
		int scoreLocation = 42; int i = 1;
		for (Integer score : scores) {
			trueTypeFont.drawString(550, scoreLocation, i+". "+score, Color.black);
			scoreLocation += 30;
			i++;
		}
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();
		
		if ((mouseX > 338 && mouseX < startButton.getWidth()+338)&&(mouseY > 190 && mouseY < startButton.getHeight()+190)) {
			if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
				PlayState play = (PlayState) (sb.getState(2));
				play.currentState = PlayState.PlayStates.NEW_GAME;
				play.score = 0;
				Pit.INSTANCE.cleanPit();
				sb.enterState(MyTetris.PLAY_STATE);
			}
			else startButton.rotate((startButtonRotate+1f)*delta);
		}
		else 
			if ((mouseX > 370 && mouseX < noButton.getWidth()+370)&&(mouseY > 473 && mouseY < noButton.getHeight()+473)) {
				if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
					Highscores.INSTANCE.save();
					gc.exit();
				}
				else noButton.rotate((noButtonRotate+3f)*delta);
			}
	}

	@Override
	public int getID() {
		return stateID;
	}

}
