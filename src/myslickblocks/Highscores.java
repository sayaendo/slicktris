package myslickblocks;

import java.io.*;
import java.util.ArrayList;

public enum Highscores 
{
	
	INSTANCE;
	
	ArrayList<Integer> scores = null;
	
	Highscores() {
		scores = new ArrayList<Integer>();
		
		for (int i=0; i<3; i++) {
			scores.add(10);
		}
	}
	
	public void add(int score) {
		int tempScore;
		
		for (int i=0; i<scores.size(); i++) {
			tempScore = scores.get(i);
			
			if (score > tempScore) 	{
				scores.add(i, score);
				scores.remove(scores.size()-1);
				return;
			}
		}
	}
	
	public void load() {
		try {
			File f = new File("data/highscores.txt");
			FileInputStream is = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(is);
			scores = (ArrayList<Integer>) (ois.readObject());
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void save() {
		try {
			File f = new File("data/highscores.txt");
			FileOutputStream fs = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fs);
			oos.writeObject(scores);
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
