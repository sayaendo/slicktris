# Slicktris #

This app is a Tetris clone build on the [Slick2D](http://slick.ninjacave.com/) open source library. It's a complete clone which saves the 3 top scores to a text file.

This project was completed in 2013. The Slick2D framework underwent major changes since, from being dropped to being picked up again by different people, so it may not be possible to run the game with the most recent Slick2D version without major changes.