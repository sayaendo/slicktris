package myslickblocks;

public enum Pit {
	
	INSTANCE;
	
	Block[][] blocks = new Block[12][24];
	int numOfBlocks = 0;
	
	Pit() { }
	
	public void addPiece(Piece piece) {
		for (int i=0; i<4; i++) {
			blocks[piece.getPitIndexOfBlockX(piece.blocks[i])][piece.getPitIndexOfBlockY(piece.blocks[i])] = piece.blocks[i];
		}
		numOfBlocks=0;
		for (int i=0; i<blocks.length; i++) {
			for (int j=0; j<blocks[i].length; j++) {
				if (getBlockAt(i, j) != null) {
					numOfBlocks++;
				}
			}
		}
	}
	
	private boolean checkFullLine(int line) {
		for (int i=0; i<blocks.length; i++)
			if (blocks[i][line] == null)
				return false;
		return true;
	}
	
	public int removeLines() {
		//iterates over all lines and removes them, returns a count of how many were deleted
		int count = 0;
		
		for (int i=0; i<blocks[0].length; i++)
			if (Pit.INSTANCE.checkFullLine(i)) {
				count++;
				Pit.INSTANCE.removeLine(i);
			}
		
		return count;
	}
	
	private void removeLine(int line) {
		for ( ; line > 0; line--) 
			for (int i=0; i<blocks.length; i++) {
				blocks[i][line] = blocks[i][line-1];
			}
		for (int i=0; i<blocks.length; i++)
			blocks[i][0] = null;
	}
	
	public Block getBlockAt(int x, int y) {
		return blocks[x][y];
	}
	
	public boolean canPieceGoDown(Piece piece) {
		/*for (int i=5; i<9; i++) {
			for (int j=0; j<4; j++)
				if (getBlockAt(i, j) != null)
					return false;
		}*/
		for (int k=0; k<4; k++) {
			if (piece.getPitIndexOfBlockY(piece.blocks[k]) == 23)
				return false;
			
			for (int i=0; i<blocks.length; i++) {
				for (int j=0; j<blocks[i].length; j++) {
					if (getBlockAt(i, j) != null) {
						if (piece.pitIndexX+piece.blocks[k].pieceIndexX == i)
							if (piece.pitIndexY+piece.blocks[k].pieceIndexY == j-1)
								return false;
					}	
				}
			}
		}
		return true;		
	}
	
	public boolean canPieceGoLeftRight(Piece piece) {
		for (int k=0; k<4; k++) {
			for (int i=0; i<blocks.length; i++) {
				for (int j=0; j<blocks[i].length; j++) {
					if (getBlockAt(i, j) != null) {
						if ((piece.pitIndexX+piece.blocks[k].pieceIndexX == i+1)||(piece.pitIndexX+piece.blocks[k].pieceIndexX == i-1))
							if (piece.pitIndexY+piece.blocks[k].pieceIndexY == j)
								return false;
					}	
				}
			}
		}
		return true;
	}
	
	public void cleanPit() {
		blocks = new Block[12][24];
	}
	
	public boolean canPieceMovev2(Piece piece) {
		for (int i=0; i<4; i++) {
			if (blocks[i+5][i] != null)
				return false;
		}
		for (int i=0; i<4; i++) {
			if (piece.getPitIndexOfBlockY(piece.blocks[i]) == 23)
				return false;
			if (blocks[piece.pitIndexX+piece.blocks[i].pieceIndexX][piece.pitIndexY+piece.blocks[i].pieceIndexY+1] != null)
				return false;
			/*if (blocks[piece.pitIndexX+piece.blocks[i].pieceIndexX+1][piece.pitIndexY+piece.blocks[i].pieceIndexY] != null)
				return false;
			if (blocks[piece.pitIndexX+piece.blocks[i].pieceIndexX-1][piece.pitIndexY+piece.blocks[i].pieceIndexY] != null)
				return false;*/
			if (blocks[piece.pitIndexX+piece.blocks[i].pieceIndexX][piece.pitIndexY+piece.blocks[i].pieceIndexY] != null)
				return false;
		}
		return true;
	}
	

}
