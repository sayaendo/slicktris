package myslickblocks;

import java.awt.Color;

public class Block {
	
	int pieceIndexX; int pieceIndexY;
	
	Color color;
	
	public Block(int x, int y) {
		pieceIndexX = x;
		pieceIndexY = y;
		
		int randomColor = (int) (Math.random()*4);
		switch (randomColor) {
			case 0: {
				color = Color.RED;
				break;
			}
			case 1: {
				color = Color.CYAN;
				break;
			}
			case 2: {
				color = Color.YELLOW;
				break;
			}
			case 3: {
				color = Color.BLUE;
				break;
			}
		}
	}

}
