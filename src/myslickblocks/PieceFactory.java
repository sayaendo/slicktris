package myslickblocks;

public class PieceFactory {
	
	private PieceFactory() { }
	
	private static Block[][][] database = new Block[][][] {
		{
			{new Block(2, 0), new Block(2, 1), new Block(2, 2), new Block(2, 3)},
			{new Block(0, 2), new Block(1, 2), new Block(2, 2), new Block(3, 2)},
			{new Block(1, 0), new Block(1, 1), new Block(1, 2), new Block(1, 3)},
			{new Block(0, 1), new Block(1, 1), new Block(2, 1), new Block(3, 1)},
		},
		{
			{new Block(1, 0), new Block(2, 0), new Block(1, 1), new Block(2, 1)},
			{new Block(1, 0), new Block(2, 0), new Block(1, 1), new Block(2, 1)},
			{new Block(1, 0), new Block(2, 0), new Block(1, 1), new Block(2, 1)},
			{new Block(1, 0), new Block(2, 0), new Block(1, 1), new Block(2, 1)},
		},
		{
			{new Block(2, 1), new Block(1, 2), new Block(2, 2), new Block(3, 2)},
			{new Block(2, 1), new Block(1, 0), new Block(1, 1), new Block(1, 2)},
			{new Block(2, 1), new Block(1, 0), new Block(2, 0), new Block(3, 0)},
			{new Block(2, 1), new Block(3, 0), new Block(3, 1), new Block(3, 2)},
		},
		{
			{new Block(2, 0), new Block(3, 0), new Block(1, 1), new Block(2, 1)},
			{new Block(2, 0), new Block(2, 1), new Block(3, 1), new Block(3, 2)},
			{new Block(3, 0), new Block(2, 0), new Block(2, 1), new Block(1, 1)},
			{new Block(2, 0), new Block(2, 1), new Block(3, 1), new Block(3, 2)},
		},
		{
			{new Block(1, 0), new Block(2, 0), new Block(2, 1), new Block(3, 1)},
			{new Block(2, 0), new Block(2, 1), new Block(1, 1), new Block(1, 2)},
			{new Block(1, 0), new Block(2, 0), new Block(2, 1), new Block(3, 1)},
			{new Block(2, 0), new Block(2, 1), new Block(1, 1), new Block(1, 2)},
		},
		{
			{new Block(2, 0), new Block(2, 1), new Block(2, 2), new Block(1, 0)},
			{new Block(0, 1), new Block(1, 1), new Block(2, 1), new Block(2, 0)},
			{new Block(1, 0), new Block(1, 1), new Block(1, 2), new Block(2, 2)},
			{new Block(1, 0), new Block(2, 0), new Block(3, 0), new Block(1, 1)},
		},
		{
			{new Block(1, 0), new Block(1, 1), new Block(1, 2), new Block(2, 0)},
			{new Block(2, 1), new Block(2, 0), new Block(1, 0), new Block(0, 0)},
			{new Block(2, 0), new Block(2, 1), new Block(2, 2), new Block(1, 2)},
			{new Block(1, 0), new Block(1, 1), new Block(2, 1), new Block(3, 1)},
		}
	};
	
	
	public static Piece generatePiece() {
		int type = (int) (Math.random()*7);
		int rotation = (int) (Math.random()*4);
		Piece piece = new Piece( type, rotation, database[type][rotation] );
		
		return piece;
	}
	
	public static void rotate(Piece piece) {
		if (piece.rotation == 3)
			piece.rotation = 0;
		else piece.rotation++;
		piece.blocks = database[piece.type][piece.rotation];
	}

}
