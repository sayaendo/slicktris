package myslickblocks;

import java.awt.Color;
import java.awt.Font;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;



//---------------------------------------------------------------
// *. lines don't clear after stacking them (probably completed line detection)
// *. doesn't transfer to game over when losing
// *. no collision detection to the left or right of blocks in pit
// *. high scores don't save/load
// 5. when there are blocks to the left of the piece can't go right & vice versa
//---------------------------------------------------------------


public class PlayState extends BasicGameState {
	
	Image gameplay, textures;
	Image yellowBlock, redBlock, cyanBlock, blueBlock;
	Image[] words = new Image[8];
	int wordsX, wordsY;
	int wordIndex;
	TrueTypeFont trueTypeFont;
	
	Piece currentPiece, nextPiece;
	int score = 0;
	boolean firstLineCleared;
	
	int deltaCounter = 500;
    int inputDelta = 0;
	
	final int PIT_LOCATION_X = 34;
	final int PIT_LOCATION_Y = 25;
	final int BLOCK_LENGTH = 24;
	final int NEXT_PIECE_LOCATION_X = 435;
	final int NEXT_PIECE_LOCATION_Y = 78;
	
	int pitCursorX, pitCursorY;
	
	int stateID;
	
	public enum PlayStates {
		NEW_GAME, NEW_PIECE, MOVING_PIECE, COMPLETED_LINE, GAME_OVER, PAUSE, HIGH_SCORE;		
	};
	
	PlayStates currentState = PlayStates.NEW_GAME;
	PlayStates pausedState;
	
	public PlayState (int id) {
		this.stateID = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sb)
			throws SlickException {
		gameplay = new Image("data/mygameplay.jpg");
		textures = new Image("data/mygametextures.jpg");
		redBlock = textures.getSubImage(14, 26, 24, 24);
		cyanBlock = textures.getSubImage(14+7+24, 26, 24, 24);
		yellowBlock = textures.getSubImage(14+14+24+24, 26, 24, 24);
		blueBlock = textures.getSubImage(14+22+24+24+24, 26, 24, 24);
		words[0] = textures.getSubImage(15, 66, 89, 39);
		words[1] = textures.getSubImage(109, 67, 91, 43);
		words[2] = textures.getSubImage(201, 55, 122, 86);
		words[3] = textures.getSubImage(59, 137, 184, 61);
		words[4] = textures.getSubImage(14, 214, 109, 43);
		words[5] = textures.getSubImage(202, 225, 108, 54);
		words[6] = textures.getSubImage(34, 277, 266, 45);
		words[7] = textures.getSubImage(27, 330, 287, 153);
		
		Font font = new Font("Arial", Font.BOLD, 20);
		trueTypeFont = new TrueTypeFont(font, true);
		
		currentState = PlayStates.NEW_GAME;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sb, Graphics g)
			throws SlickException {
		gameplay.draw(0, 0);
		words[wordIndex].draw(wordsX, wordsY);
		drawCurrentPiece();
		drawNextPiece();
		drawPit();
		
		trueTypeFont.drawString(440, 308, score+" ", org.newdawn.slick.Color.black);
	}

	

	@Override
	public void update(GameContainer gc, StateBasedGame sb, int delta)
			throws SlickException {
		
		if (firstLineCleared) {
			wordsX = (int) (Math.random()*549);
			wordsY = (int) (Math.random()*552);
		} else {
			wordsX = (int) (Math.random()*177+361);
			wordsY = (int) (Math.random()*164+390);
		}
		wordIndex = (int) (Math.random() * 7);
		
		Input input = gc.getInput();
		if (input.isKeyPressed(Input.KEY_SPACE)) {
			if (currentState == PlayStates.PAUSE)
				currentState = pausedState;
			else {
				pausedState = currentState;
				currentState = PlayStates.PAUSE;
			}
		}
		
		switch (currentState) 
		{
			case NEW_GAME: { newGame(); break; }
			case NEW_PIECE: { newPiece(); break; }
			case MOVING_PIECE: { movingPiece(gc, sb, delta); break; }
			case COMPLETED_LINE: { completedLine(); break; }
			case GAME_OVER: { gameOver(); break; }
			case PAUSE: { pause(); break; }
			case HIGH_SCORE: { highScore(gc, sb); break; }		
		}
	}
	
	private void highScore(GameContainer gc, StateBasedGame sb) {
		Highscores.INSTANCE.add(score);
		Highscores.INSTANCE.save();
		
		sb.enterState(MyTetris.MENU_STATE);
	}

	private void pause() {
		
	}

	private void gameOver() {
		currentState = PlayStates.HIGH_SCORE;
	}
	
	private void completedLine() {
		int linesRemoved = Pit.INSTANCE.removeLines();
		switch (linesRemoved) {
		case 0: break;
		case 1: { score+=100; break; }
		case 2: { score+=300; break; }
		case 3: { score+=1000; break; } 
		case 4: { score+=3000; break; }
		}
		
		currentState = PlayStates.NEW_PIECE;
	}

	private void movingPiece(GameContainer gc, StateBasedGame sb, int delta) {	    
		    deltaCounter -= delta;
		    inputDelta -= delta;
		    
		    if (deltaCounter < 0) {
		    	if (!Pit.INSTANCE.canPieceMovev2(currentPiece)) {
					score+=10;
					Pit.INSTANCE.addPiece(currentPiece);
					
					currentState = PlayStates.COMPLETED_LINE;
				}
		    	currentPiece.setPitIndex(currentPiece.pitIndexX, currentPiece.pitIndexY+1);
		    	deltaCounter = 500;
		    }
		    
		    if (inputDelta < 0) {
		    	Input input = gc.getInput();
				if (input.isKeyDown(Input.KEY_DOWN)) {
					if (Pit.INSTANCE.canPieceMovev2(currentPiece)) {
						currentPiece.setPitIndex(currentPiece.pitIndexX, currentPiece.pitIndexY+1);
					}
					inputDelta = 10;
				}
				if (input.isKeyPressed(Input.KEY_LEFT)) {
					boolean flag = true;
					for (int i=0; i<4; i++) {
						if (currentPiece.getPitIndexOfBlockX(currentPiece.blocks[i]) == 0)
							flag = false;
					}
					if (!Pit.INSTANCE.canPieceGoLeftRight(currentPiece))
						flag = false;
					if (flag) {
						currentPiece.setPitIndex(currentPiece.pitIndexX-1, currentPiece.pitIndexY);
						inputDelta = 100;
					}
				}
				if (input.isKeyPressed(Input.KEY_RIGHT)) {
					boolean flag = true;
					for (int i=0; i<4; i++) {
						if (currentPiece.getPitIndexOfBlockX(currentPiece.blocks[i]) == 11)
							flag = false;
					}
					if (!Pit.INSTANCE.canPieceGoLeftRight(currentPiece))
						flag = false;
					if (flag) {
						currentPiece.setPitIndex(currentPiece.pitIndexX+1, currentPiece.pitIndexY);
						inputDelta = 100;
					}
				}
				if (input.isKeyPressed(Input.KEY_UP)) {
					PieceFactory.rotate(currentPiece);
					inputDelta = 150;
				}
		    }
	}

	private void newPiece() {
		
		//if (currentPiece == null)
        //    nextPiece = PieceFactory.generatePiece();
        currentPiece = nextPiece;
        currentPiece.setPitIndex(4, 0);
        nextPiece = PieceFactory.generatePiece();
  
        if (Pit.INSTANCE.canPieceMovev2(currentPiece))
        {
            //nextPiece = PieceFactory.generatePiece();
            currentState = PlayStates.MOVING_PIECE;
        }else{
            currentState = PlayStates.GAME_OVER;
        }
		/*
		currentPiece = nextPiece;
		currentPiece.setPitIndex(4, 0);
		nextPiece = PieceFactory.generatePiece();
		
		if (!Pit.INSTANCE.canPieceMovev2(currentPiece)) {
			currentState = PlayStates.GAME_OVER;
		}
		
		currentState = PlayStates.MOVING_PIECE;*/
	}

	private void newGame() {
		nextPiece = PieceFactory.generatePiece();
		currentPiece = PieceFactory.generatePiece();
		currentPiece.setPitIndex(4, 0);
		
		currentState = PlayStates.MOVING_PIECE;
	}
	
	private void drawCurrentPiece() {
		Image tempBlock;
		
		for (int i=0; i<4; i++) {
			tempBlock = blockImage(currentPiece.blocks[i]);
			tempBlock.draw( PIT_LOCATION_X + (currentPiece.getPitIndexOfBlockX(currentPiece.blocks[i]) * BLOCK_LENGTH )  , 
					PIT_LOCATION_Y + (currentPiece.getPitIndexOfBlockY(currentPiece.blocks[i]) * BLOCK_LENGTH ) +1);
		}
	}
	
	private void drawNextPiece() {
		Image tempBlock;
		
		for (int i=0; i<4; i++) {
			tempBlock = blockImage(nextPiece.blocks[i]);
			tempBlock.draw( NEXT_PIECE_LOCATION_X + (nextPiece.blocks[i].pieceIndexX * BLOCK_LENGTH) ,
					NEXT_PIECE_LOCATION_Y + (nextPiece.blocks[i].pieceIndexY * BLOCK_LENGTH));
		}
	}
	
	private void drawPit() {
		Block tempBlock;
		Image tempBlockImage;
		for (int i=0; i<Pit.INSTANCE.blocks.length; i++) {
			for (int j=0; j<Pit.INSTANCE.blocks[i].length; j++) {
				tempBlock = Pit.INSTANCE.blocks[i][j];
				if (tempBlock != null) {
					tempBlockImage = blockImage(tempBlock);
					tempBlockImage.draw( PIT_LOCATION_X + (i * BLOCK_LENGTH), PIT_LOCATION_Y + (j * BLOCK_LENGTH) );
				}
			}
		}
	}
	
	private Image blockImage(Block block) {
		if (block.color == Color.RED)
			return redBlock;
		if (block.color == Color.YELLOW)
			return yellowBlock;
		if (block.color == Color.BLUE)
			return blueBlock;
		return cyanBlock;
	}
	


	

	@Override
	public int getID() {
		return stateID;
	}

}
