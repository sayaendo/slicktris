package myslickblocks;

public class Piece {
	int pitIndexX, pitIndexY;
	int type;
	int rotation;
	Block[] blocks;
	
	public Piece(int type, int rotation, Block[] blocks) {
		this.type = type;
		this.blocks = blocks;
		this.rotation = rotation;
	}
	
	public int getPitIndexOfBlockX(Block block) {
		return pitIndexX+block.pieceIndexX;
	}
	
	public int getPitIndexOfBlockY(Block block) {
		return pitIndexY+block.pieceIndexY;
	}
	
	public void setPitIndex(int x, int y) {
		pitIndexX = x;
		pitIndexY = y;
	}
	
}
